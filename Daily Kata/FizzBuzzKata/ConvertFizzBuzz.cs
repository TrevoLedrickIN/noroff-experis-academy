﻿using System;

namespace FizzBuzzKata
{
    public class ConvertFizzBuzz
    {
        public static void FizzBuzzConverter()
        {
            string Fizz = "Fizz";
            string Buzz = "Buzz";
            string FizzBuzz = "FizzBuzz";

            for (int i = 1; i <= 100; i++)
            {
                // Another way to solve this problem:

                //switch(i % 3 == 0 && i % 5 == 0 ? "FizzBuzz" :
                //       i % 3 == 0 ? "Fizz" :
                //       i % 5 == 0 ? "Buzz" :
                //       "Number")
                //{
                //    case "FizzBuzz":
                //        Console.WriteLine(FizzBuzz);
                //        break;
                //    case "Fizz":
                //        Console.WriteLine(Fizz);
                //        break;
                //    case "Buzz":
                //        Console.WriteLine(Buzz);
                //        break;
                //    case "Number":
                //        Console.WriteLine(i.ToString());
                //        break;
                //}

                if (i % 3 == 0 && i % 5 == 0)
                {
                    Console.WriteLine(FizzBuzz);
                }
                else if (i % 3 == 0)
                {
                    Console.WriteLine(Fizz);
                }
                else if (i % 5 == 0)
                {
                    Console.WriteLine(Buzz);
                }
                else
                {
                    Console.WriteLine(i.ToString());
                }
            }
        }
        static void Main(string[] args)
        {
            FizzBuzzConverter();
        }
    }
}
